<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomePage@index');
Route::get('/post', 'HomePage@post');
Route::get('/registration', 'HomePage@registrationfrom')->name('register');
Route::post('/registration', 'HomePage@registerProcess');

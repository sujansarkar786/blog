<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomePage extends Controller
{
    public function index(){
        return view('index');
    }
    public function post(){
        return view('frontend.post');
    }
    public function registrationfrom(){
        return view('frontend.registration');
    }
    public function registerProcess(Request $request){
       return $request->all();
    }
}

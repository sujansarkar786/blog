@extends('master')
@section('post')
<div class="container">
    <div class="col-md-12">
    <form action="{{route('register') }}" method="post">
        @csrf
            <div class="form-group">
                <label for="email">Enter your Name:</label>
                <input type="text" class="form-control" name="first_name" placeholder="Enter email" id="email">
              </div>
            <div class="form-group">
              <label for="email">Email address:</label>
              <input type="email" class="form-control" name="email_address" placeholder="Enter email" id="email">
            </div>
            <div class="form-group">
              <label for="pwd">Password:</label>
              <input type="password" class="form-control" name="password" placeholder="Enter password" id="pwd">
            </div>
            <div class="form-group form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Remember me
              </label>
            </div>
            <button type="submit" name="btn" class="btn btn-primary">Submit</button>
          </form>
    </div>
</div>


@endsection
